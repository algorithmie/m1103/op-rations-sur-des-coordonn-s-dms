class DegreSexa():
    def __init__(self, degres=00, minute=00, seconde=00):
        self.d = degres
        self.m = minute
        self.s = seconde


def print_lat(l: DegreSexa):
    print("{:02d}°{:02d}'{:02d}'' ".format(l.d, l.m, l.s))


def sexa2Dec(A: DegreSexa) -> float:
    latitude: float
    latitude = A.d + (A.m / 60) + (A.s / 3600)
    return latitude


def dec2Sexa(A: float):
    ld = int(A)
    lm = (A - ld) * 60
    ls = (lm - int(lm)) * 60

    lm = int(lm)
    ls = int(ls)

    return print_lat((DegreSexa(ld, lm, ls)))


def Suiv(latSuiv: DegreSexa) -> DegreSexa:
    latSuiv.s += 1

    if latSuiv.s >= 60:
        latSuiv.m += 1
        latSuiv.s = 00

    if latSuiv.m >= 60:
        latSuiv.d += 1
        latSuiv.m = 00

    if latSuiv.d >= 360:
        latSuiv.d = 00

    return (print_lat(latSuiv))

if (__name__ == "__main__"):
    print(" | --------------------- | ")
    print(" | Conversion lat / dec  | ")
    print(" |–––––––––––––––––––––––| ")
    print(" |           MENU        | ")
    print(" | 1)Entrer une latitude | ")
    print(" | 2)Entrer une valeur   | ")
    print(" | 3)Suivant             | ")
    print(" | --------------------- |\n ")
    choix = int(input("Votre choix –>", ))
    if choix == 1:
        lat = DegreSexa()
        lat.d = int(input("Degrés >>> "))
        lat.m = int(input("Minutes >>> "))
        lat.s = int(input("Secondes >>> "))
        print_lat(lat)
        print("Convertion: \n")
        print(sexa2Dec(lat))
    elif choix == 2:
        lat: float
        lat = float(input(">>> "))
        print_lat(dec2Sexa(lat))
    elif choix == 3:
        print("Tous d'abbord veuillez définir une latitude: ")
        lat2 = DegreSexa()
        lat2.d = int(input("Degrés >>> "))
        lat2.m = int(input("Minutes >>> "))
        lat2.s = int(input("Secondes >>> "))
        print("Votre latitude: ",end="")
        print_lat(lat2)
        turn=int(input("Combien de fois faut-il ajouter 1 seconde ?"))
        for i in range (0,turn):
            Suiv(lat2)
        print("Votre latitude mise à jour: ", end="")
        print_lat(lat2)
    elif choix > 3:
        print("Erreur, choix non valide !")